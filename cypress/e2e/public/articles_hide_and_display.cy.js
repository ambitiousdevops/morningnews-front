/// <reference types="cypress" />

describe('Article - Show All Functionality', () => {

  it('should hide a random article and then show all articles again', () => {

    // the home page is where all the articles are displayed (9 max)
    cy.visit(Cypress.env('baseUrl'));

    cy.get('[class*="Article_articles__"]').then(($articles) => {
      // check that there are 9 articles, using the class
      expect($articles).to.have.length(9);

      // select a random article
      const randomIndex = Math.floor(Math.random() * $articles.length);
      const randomArticle = $articles.eq(randomIndex);

      // it should be visible
      cy.wrap(randomArticle).should('be.visible');

      // use the text as a unique identifier (maybe a better option exists ?)
      const articleText = randomArticle.text().trim();

      // click on the icon to hide it
      cy.wrap(randomArticle)
        .find('.fa-eye-slash')
        .click();

      // checking that the article is hiden using it's text content
      cy.contains(articleText).should('not.exist');

      // display again all article using the general option in the header
      cy.get('[class*="Header_unhideIcon__"]').click();

      // check that we have now 9 articles
      cy.get('[class*="Article_articles__"]').should('have.length', 9).each(($article) => {
        cy.wrap($article).should('be.visible');
      });
    });
  });
});
