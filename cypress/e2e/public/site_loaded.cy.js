/// <reference types="cypress" />
import moment from 'moment';


describe('Homepage - Articles', () => {
  
    it('should display the top article when the backend is up', () => {
      // reach the main page
      cy.visit(Cypress.env('baseUrl'));

      cy.get('[class*="TopArticle_topContainer__"] .TopArticle_image__br_jk')
        .should('be.visible')
  
      // main article exists
      cy.get('[class*="TopArticle_topContainer__"] h2')
        .should('be.visible')
        .and('not.be.empty');
  
      // author exists
      cy.get('[class*="TopArticle_topContainer__"] h4')
        .should('be.visible')
        .and('not.be.empty');
  
      // short text exists
      cy.get('[class*="TopArticle_topContainer__"] p')
        .should('be.visible')
        .and('not.be.empty');
    });
});

describe('Header - Date', () => {
  
  it('should display a data which is the current date', () => {
    cy.visit(Cypress.env('baseUrl'));

    // get the current date
    const formattedDate = moment().format('MMM Do YYYY');

    // check that the element exists and contain the formattedDate
    cy.get('[class*="Header_date__"]')
      .should('exist')
      .and('be.visible')
      .and('contain', formattedDate);
  });
});

describe('Bookmarks', () => {
  
  it('should display an empty Bookmarks page', () => {
    cy.visit(Cypress.env('baseUrl'));
    // get the current date

    cy.get('[class*="Header_linkContainer__"] > :nth-child(2)').click()

    cy.get('[class*="Bookmarks_title__"]')
      .should('exist')
      .and('be.visible')
      .and(($el) => {
        expect($el).to.have.text;
        expect($el.text().trim()).not.to.be.empty;
    });

    cy.get('[class*="Bookmarks_articlesContainer__"] > p')
      .should('exist')
      .and('be.visible')
      .and(($el) => {
        expect($el).to.have.text;
        expect($el.text().trim()).not.to.be.empty;
    });

  });
});
