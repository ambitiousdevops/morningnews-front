/// <reference types="cypress" />

describe('Navigation - Menu Links', () => {
    before(() => {
        // will visit the home page before runing the following tests
        cy.visit(Cypress.env('baseUrl'));
      });
  
    it('should navigate to the Bookmark page when clicking on "Bookmark"', () => {
      // click on bookmark link
      cy.contains('Bookmark').click();
  
      // the url should be /bookmark
      cy.url().should('include', '/bookmark');
  
      // check that the content is loaded
      cy.get('[class*="Bookmarks_title__"]').should('be.visible');


      cy.contains('Articles').click();
  
      // the url should be /baseUrl (given by the .env)
      cy.url().should('eq', Cypress.env('baseUrl'));
  
      // check that the content is loaded and that all articles are displayed
      cy.get('[class*="Article_articles__"]').should('have.length', 9);

    });
  
  });
  